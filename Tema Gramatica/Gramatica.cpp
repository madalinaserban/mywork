#include "Gramatica.h"
#include<fstream>
#include<iostream>
#include<algorithm>
//#include<unordered_set>
#include<time.h>
std::ifstream f("date.in");
Gramatica::Gramatica()
{
	m_Vn = " ";
	m_Vt = " ";
	m_S = " ";
	numar_productii = 0;
	//setCuvinteFinale();
	CuvintecuPasi.clear();
	m_n = 0;
}
Gramatica::Gramatica(std::string Vn, std::string Vt, std::string S)
{
	m_Vn = Vn;
	m_Vt = Vt;
	m_S = S;
}
void Gramatica::citire(int n)
{
	std::string x;
	std::string productie;
	std::string a;
	std::string b;
	f >> x;
	m_Vn = x;
	f >> x;
	m_Vt = x;
	f >> x;
	m_S = x;
	int nr;
	f >> nr;
	numar_productii = nr;
	P pr;
	m_n = n;
	for (int i = 0; i < numar_productii; i++)
	{
		vectorProductii.push_back(pr);
		f >> productie;
		for (int j = 0; j < productie.size(); j++)
		{
			if (productie[j] == '-')
			{
				for (int z = j + 2; z < productie.size(); z++)
				{
					vectorProductii[i].dreapta += productie[z];
				}
				break;
			}
			vectorProductii[i].stanga += productie[j];
		}

	}
}
void  Gramatica::afisare()
{
	std::cout << "_________" << "Afisare" << "__________" << "\n";
	std::cout << "Neterminale : " << m_Vn << "\n" << "Terminale : " << m_Vt << "\n" << "Start : " << m_S;
	std::cout << '\n' << "Productii:";
	getProductii();
}
void Gramatica::getProductii()
{
	for (int i = 0; i < vectorProductii.size(); i++)
		std::cout << vectorProductii[i].stanga << " -> " << vectorProductii[i].dreapta << '\n';

}
void Gramatica::Generare(bool x)
{
	std::string S;
	S= m_S;
	std::vector <P> ProductiiAplicabile;
	do {

		ProductiiAplicabile.resize(0);
		for (int i = 0; i < vectorProductii.size(); i++)
		{
			if (S.find(vectorProductii[i].stanga) != std::string::npos)
			{
				ProductiiAplicabile.push_back(vectorProductii[i]);
			}
		
		}
		
		if (ProductiiAplicabile.size() != 0)
		{
			int random = RandomModern(ProductiiAplicabile.size());
			int pos = S.find(ProductiiAplicabile[random].stanga);
			if (x == 1)
			{
				std::cout << ProductiiAplicabile[random].stanga << "->" << ProductiiAplicabile[random].dreapta << "/";
			}
			if (ProductiiAplicabile[random].dreapta != "$")
			{
				S.replace(pos, ProductiiAplicabile[random].stanga.size(), ProductiiAplicabile[random].dreapta);
				if (x == 1)
				{
					std::cout << S << "  ";
				}
			}
			if (ProductiiAplicabile[random].dreapta == "$")
			{
				S.erase(pos, ProductiiAplicabile[random].stanga.size());
				if (x == 1)
				{
					std::cout << S << " ";
				}
			}
		}
	} while (ProductiiAplicabile.size()!=0);
	if (x==0 && AreDoarTerminale(S)==0)
	{ 
		std::cout << S;
	}
}

std::string Gramatica::getS()
{
	return m_S;

}

std::string Gramatica::getVn()
{
	return m_Vn;
}
std::string Gramatica::getVt()
{
	return m_Vt;
}
//void Gramatica::CuvinteFinale(std::vector<std::string> CuvFinale)
//{
//
//	for (int i = 0; i < CuvFinale.size() - 1; i++)
//	{
//		int numara = 0;
//		for (int j = i + 1; j < CuvFinale.size(); j++)
//		{
//			if (CuvFinale[i] == CuvFinale[j])
//			{
//				numara++;
//
//			}
//
//		}
//		if (numara == 0)
//			std::cout << "->" << CuvFinale[i] << "\n";
//	}
//}
//void Gramatica::AfisarePasi(std::vector<std::string>& CuvintecuPasi)
//{
//	std::cout << m_S << " -> ";
//	for (int i = 0; i < CuvintecuPasi.size() - 1; i++)
//		std::cout << CuvintecuPasi[i] << " ";
//
//	//std::cout << CuvintecuPasi[CuvintecuPasi.size()-1];
//
//	CuvintecuPasi.clear();
//}
//bool Gramatica::VerificareRepetareCuvinte(std::string cuvant)
//{ //aici nu inteleg de ce nu merge , apare ca primul cuvant se repeta desi nu are cum
//	int SeRepeta = 0;
//	for (int i = 0; i < CuvFinale.size(); i++)
//	{
//		if (cuvant == CuvFinale[i])
//		{
//			SeRepeta++;
//		}
//	}
//	if (SeRepeta != 0)
//	{
//		return false; // se repeta
//	}
//	else return true; //nu se repeta
//}
//void Gramatica::setCuvinteFinale()
//{
//	CuvFinale.clear();
//}
void Gramatica::verificare( bool &eroare)
{
	std::string Vn = m_Vn;
	std::string Vt = m_Vt;
	std::string S = m_S;
	//1. Verifica daca Vn intersectat cu Vt e vid
	for (int i = 0; i < Vn.size(); i++)
	{
		for (int j = 0; j < Vt.size(); j++)
			if (Vn[i] == Vt[j])
			{
				eroare = 1;
				std::cout << "!!!ERORE : Vn intersectat cu Vt nu e vid";
				break;

			}
	}

	int numara = 0;
	//2.verificare S apartine Vn
	for (int i = 0; i < S.size(); i++)
	{
		for (int j = 0; j < Vn.size(); j++)
		{
			if (S[i] == Vn[j])
				numara++;
		}

	}
	if (numara != S.size())
	{
		std::cout << '\n';
		std::cout << "!!! Eroare : S nu apartine Vn ";
		eroare = 1;
	}

	//3.membrul stang contine cel putin un neterminal
	P productie;
	for (int i = 0; i < vectorProductii.size(); i++)
	{
		numara = 0;
		for (int j = 0; j < vectorProductii[i].stanga.size(); j++)
		{
			for (int z = 0; z < Vn.size(); z++)
			{
				if (vectorProductii[i].stanga[j] == Vn[z])
				{
					numara++;
				}

			}
		}
		if (numara < 1)
		{
			std::cout << "\n" << "!!!EROARE: exista reguli care nu au in membrul stang cel putin un neterminal";
			eroare = 1;
		}


	}
	//4.Exista cel putin o productie care in stanga are doar S
	int numarareS = 0;
	for (int i = 0; i < vectorProductii.size(); i++)
	{
		int numara = 0;
		for (int j = 0; j < vectorProductii[i].stanga.size(); j++)
		{
			for (int z = 0; z < S.size(); z++)
			{
				if (vectorProductii[i].stanga[j] == S[z])
				{
					numara++;
				}

			}
			if (numara == vectorProductii[i].stanga.size())
			{
				numarareS++;
			}
		}

	}
	if (numarareS == 0)
	{
		std::cout << "\n" << "!!!EROARE: nicio regula nu are doar S in stanga ";
		eroare = 1;
	}
	//5.Fiecare productie are doar elemente din Vn si Vt
	std::string lipire = Vn;
	lipire += Vt;
	numara = 0;
	for (int i = 0; i < vectorProductii.size(); i++) //parcurg productii
	{
		std::string pr = vectorProductii[i].dreapta; //lipesc productia
		pr += vectorProductii[i].stanga;
		for (int j = 0; j < lipire.size(); j++)
		{
			if (pr.find(lipire[j])!=std::string::npos) 
			{     
				for (int k = 0; k < pr.size(); k++)
				{
					if (pr[k] == lipire[j])
					{
						pr.erase(std::remove(pr.begin(), pr.end(), pr[k]), pr.end());
					}
				}
			  
			}
		}

		if (pr.size()!=0)
		{
			std::cout << "\n !!! EROARE elemente neconforme ";
			eroare = 1;
			break;
		}

	}

}
bool Gramatica::arecuvdeStart(std::string cuvant)
{
	
	int contor = 0;
	for (int i = 0; i < cuvant.size(); i++)
	{
		for (int j = 0; j < m_Vn.size(); j++)
		{
			if (cuvant[i] == m_Vn[j])
				contor++;
		}
	}
	if (contor > 0)
		return 1;
	else return 0;
}
bool Gramatica::AreDoarTerminale(std::string cuvant)
{

	int contor = 0;
	for (int i = 0; i < cuvant.size(); i++)
	{
		for (int j = 0; j < m_Vt.size(); j++)
		{
			if (cuvant[i] == m_Vt[j])
				contor++;
		}
	}
	if (contor == cuvant.size())
		return 0;
	else return 1;
}
int Gramatica::RandomModern(const int stop)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(0, stop - 1);
	return dist(mt);

}
