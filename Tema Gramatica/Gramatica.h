#pragma once
#include<vector>
#include<string>
#include<random>
class Gramatica
{
public: std::string  m_Vn, m_Vt, m_S;
	  int numar_productii, m_n;
	  std::vector<std::string> CuvFinale; 
	  std::vector <std::string > CuvintecuPasi;
public: struct P {
	std::string dreapta;
	std::string stanga;
};
	  std::vector<P>vectorProductii;
public:Gramatica();
	  Gramatica(std::string Vn, std::string Vt, std::string S);
	  void citire(int n);
	  void afisare();
	  void getProductii();
	  //P AlegeRandom();
	  void Generare( bool x);
	  std::string getS();
	  std::string getVn();
	  std::string getVt();
	  //void CuvinteFinale(std::vector<std::string> CuvFinale);
	  //void AfisarePasi(std::vector<std::string>& CuvintecuPasi);
	  //bool VerificareRepetareCuvinte(std::string cuvant);
	  void verificare(bool &eroare);
	  bool arecuvdeStart(std::string cuvant);
	  bool AreDoarTerminale(std::string cuvant);
	  int RandomModern(const int stop);

};