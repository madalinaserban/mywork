// AFD.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "AFD_class.h"
#include "Minimizare.h"
int main()
{   Minimizare afd;
	afd.citire();
	afd.construire_q_sigma();
	while (afd.stersStari != 0)
	{
		afd.Pas0();
	}
	afd.Pas1();
	afd.Pas2();
	afd.Pas3();
	afd.AfisareAutomatFisier();

}