#pragma once
#include<string>
#include<tuple>
#include<set>
#include<map>
#include <vector>
#include<fstream>
class AFD_class
{
public:  std::string alfabet_intrare, stareinit, cuvfinale,string_stari;
	  std::set<char> set_alfabet;
	  std::set<std::string> set_finale;
	  std::vector <std::string> stari;
	  std::pair<std::string, char> fct_tranzitie;
	  std::map<std::pair<std::string, char>, std::string> functii_Tranzitie;
	  int GetNumarStari();
	  uint16_t numarul_tranzitiilor;
public: void afisare();
	  void construire_q_sigma();
	  void citire();
};