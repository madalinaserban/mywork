﻿#include "Minimizare.h"
#include<iostream>
#include<fstream>
void Minimizare::Pas0()
{//eliminarea starilor inaccesibile
	stersStari = 0;
	for (auto st : stari)
	{
		bool numara = 0;
		for (auto index : functii_Tranzitie)
		{
			if (index.first.first != st && index.second == st)

				numara = 1;
			if (numara == 1)
				break;
		}
		if (numara == 0 && st != "" && stareinit.find(st) == std::string::npos)
		{
			Minimizare::Stergere_Inaccesibile(st);
		}
	}

}
void Minimizare::Stergere_Inaccesibile(std::string st)
{
	for (auto x : set_alfabet)
		functii_Tranzitie.erase(make_pair(st, x));
	std::cout << st << " este inaccesibila \n";
	for (int i = 0; i < stari.size(); i++)
	{
		if (stari[i] == st)
		{
			stari.erase(stari.begin() + i);

		}
	}
	stersStari = 1;


}
void Minimizare::Pas1()
{
	x = stari.size();
	std::cout << "\n _________Pas 1_________ \n";
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < i; j++)
			matrice[i][j] = "0";
	}
	for (int i = 0; i < x; i++)
	{
		matrice[i][i] = stari[i];

		if (cuvfinale.find(stari[i]) != std::string::npos) //primul este final
		{
			for (int j = 0; j < i; j++)
			{
				if (cuvfinale.find(stari[j]) == std::string::npos) //al 2 lea nu este final
				{
					matrice[i][j] = "x";
				}
			}
		}
		if (cuvfinale.find(stari[i]) == std::string::npos)
		{
			for (int j = 0; j < i; j++)
			{
				if (cuvfinale.find(stari[j]) != std::string::npos)
				{
					matrice[i][j] = "x";
				}
			}
		}
	}
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < x; j++)
		{
			std::cout << matrice[i][j];
			std::cout << " ";
		}
		std::cout << "\n";
	}
	//marchez tot ce se leaga cu starile finale
	std::cout << "\n";
}
void Minimizare::Pas2()
{
	int contor;
	std::map<std::pair<std::string, char>, std::string>::iterator it;
	std::cout << "\n _________Pas 2_________ \n";
	do {
		contor = 0;
		for (int indexi = 1; indexi < stari.size(); indexi++)
		{
			for (int indexj = 0; indexj < indexi; indexj++)
			{
				if (matrice[indexi][indexj] == "0")
				{	//trebuie sa parcurg alfabetul initial
					for (auto i : set_alfabet)
					{///!DACA NU EXISTA PERECHEA CAUTATA DA EROARE
						it = functii_Tranzitie.find(std::make_pair(stari[indexi], i));
						std::string str1 = it->second;
						it = functii_Tranzitie.find(std::make_pair(stari[indexj], i));
						std::string str2 = it->second;
						std::string DeNotat = matrice[indexi][indexj];
						for (int indexz = 0; indexz < stari.size(); indexz++)
						{
							if (stari[indexz] == str1)
							{
								Decomparat_i = indexz;
							}
							if (stari[indexz] == str2)
							{
								Decomparat_j = indexz;
							}
						}
						std::string DeComparat;
						if (Decomparat_i > Decomparat_j)
						{
							DeComparat = matrice[Decomparat_i][Decomparat_j];
						}
						else if (Decomparat_j > Decomparat_i)
						{
							DeComparat = matrice[Decomparat_j][Decomparat_i];
						}
						if (DeComparat == "x" && DeNotat == "0")
						{
							matrice[indexi][indexj] = "x";
							contor = 1;
						}

					}

				}
			}
		}
	} while (contor != 0);
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < x; j++)
		{
			std::cout << matrice[i][j];
			std::cout << " ";
		}
		std::cout << "\n";
	}
}
void Minimizare::Pas3()
{
	x = stari.size();
	bool modif = 0; int anterior;
	std::cout << "\n _________Pas 3_________ \n";
	std::map<std::pair<std::string, char>, std::string>::iterator it;
	for (int indexi = x - 1; indexi >= 0; indexi--)
	{
		echivalente.insert(indexi);
		for (int indexj = 0; indexj < indexi; indexj++)
		{
			if (matrice[indexi][indexj] == "0" && indexj < indexi)
			{
				modif = 1;
				echivalente.insert(indexj);
				stariCareauEchivalent.insert(matrice[indexi][indexi]);
				stariCareauEchivalent.insert(matrice[indexj][indexj]);

				for (auto it : echivalente)
				{
					for (auto it2 : echivalente)
					{
						if (it > it2)
						{
							matrice[it][it2] = "x";
						}
						else if (it < it2)
						{
							matrice[it2][it] = "x";
						}
					}
					for (auto it3 : set_alfabet)
						functii_Tranzitie.erase(make_pair(matrice[indexj][indexj], it3));

				}



			}
		}
		if (modif == 1)
		{
			vectorCuEchivalente.insert(echivalente);
			echivalente.clear();
		}
		modif = 0;
		echivalente.clear();
	}

	std::string cuvinsert;
	for (auto a : vectorCuEchivalente)
	{
		for (auto x : a)
		{
			std::cout << matrice[x][x] << " ";
			cuvinsert += matrice[x][x];
		}
		std::cout << std::endl;
		finaleechivalenta.insert(cuvinsert);
		cuvinsert.clear();
	}
	
}
void Minimizare::AfisareAutomatFisier()
{
	//afisare();
	std::cout << std::endl;
	std::string tranzitie1, tranzitie2;
	std::map<std::pair<std::string, char>, std::string> functii_Tranzitie2;
	//ToDO: parcurg functii_Tranzitie1 , caut in stari care au echivalenta , daca se afla acolo trebuie inlocuit
	for (auto it = functii_Tranzitie.begin(); it != functii_Tranzitie.end(); ++it)
	{
		if (stariCareauEchivalent.find(it->first.first) != stariCareauEchivalent.end())

		{
			for (auto it3 : finaleechivalenta)
			{
				if (it3.find(it->first.first) != std::string::npos)
				{
					tranzitie1 = it3;


				}
			}
		}

		else tranzitie1 = it->first.first;
		if (stariCareauEchivalent.find(it->second) != stariCareauEchivalent.end())

		{
			for (auto it3 : finaleechivalenta)
			{
				if (it3.find(it->second) != std::string::npos)
				{
					tranzitie2 = it3;


				}
			}
		}
		else tranzitie2 = it->second;
	
			functii_Tranzitie2.insert({ make_pair(tranzitie1,it->first.second),tranzitie2 });
		





	}
	std::ofstream fout("AutomatNou.out");
	std::set<std::string>set_tranzitii2;
	for (auto it = functii_Tranzitie2.begin(); it != functii_Tranzitie2.end(); ++it)
	{
		fout << it->first.first<<",";
		++it;
		set_tranzitii2.insert(it->first.first);
	}
	fout << "\n";
	for (auto x : set_tranzitii2)
	{
		if (x.find(stareinit) != std::string::npos)
			fout << x<<"\n";
		for (auto y : set_finale)
		{
			if (x.find(y) != std::string::npos)
			{
				fout << x << ",";
				x.erase(x.find(y));
			}
		}
	}

	fout <<"\n"<< functii_Tranzitie2.size()<<"\n";
	for (auto it = functii_Tranzitie2.begin(); it != functii_Tranzitie2.end(); ++it)
	{
		fout << it->first.first << " " << it->first.second << " " << it->second;
		fout << "\n";
	}
	
}
