#pragma once
#include<string>
#include<tuple>
#include<set>
#include<map>
class AFDclass
{
public:  std::string alfabet_intrare, stareinit,cuvfinale;
	 std::set<char> set_alfabet;
	  std::set<std::string> stari;
	  std::pair<std::string, char> fct_tranzitie;
	  std::map<std::pair<std::string, char> , std::string> functii_Tranzitie;

	  uint16_t numarul_tranzitiilor;
public: void afisare();
	  void construire_q_sigma();
	  void accepta(std::string cuvant);
	  void verifica(bool &eroare);
	  AFDclass();
	  void citire();
};

