#include "AFDclass.h"
#include<fstream>
#include<string>
#include<iostream>
#include<map>
#include<stack>
std::ifstream f("DateAFD.in");

void AFDclass::afisare()
{
	std::cout << "Q : ";
	for (auto it = stari.begin(); it != stari.end(); ++it)
	{
		std::cout << ' ' << *it;
	}
	std::cout << "\n";
	std::cout << "Sigma : ";
	for (auto it = set_alfabet.begin(); it != set_alfabet.end(); ++it)
	{
		std::cout << ' ' << *it;
	}std::cout << "\n";
	//alfabetul de intrare
	std::cout << "Starea initiala : " << stareinit << "\n";
	std::cout << "Multimea starilor finale : "<<cuvfinale<<"\n";
	std::cout << " Tranzitii : " << "\n";
	for (auto it = functii_Tranzitie.begin(); it != functii_Tranzitie.end(); ++it)
	{
		std::cout << it->first.first << " " << it->first.second << " " << it->second;
		std::cout << "\n";
	}

}

void AFDclass::construire_q_sigma()
{
	for (auto it = functii_Tranzitie.begin(); it != functii_Tranzitie.end(); ++it)
	{
		stari.insert(it->first.first);
		stari.insert(it->second);
		set_alfabet.insert(it->first.second);
	}
}

void AFDclass::accepta(std::string cuvant)
{
	std::stack<char> stackcuvant;
	std::map<std::pair<std::string, char>, std::string>::iterator it;
	char litera;
	std::string tranzitie;
	tranzitie = stareinit;
	bool blocaj = 0;
	for (int i = cuvant.size() - 1; i >= 0; i--)
	{
		stackcuvant.push(cuvant[i]);
	}
	//cat timp stiva nu este goala && nu exista blocaj
	while (!stackcuvant.empty()&& blocaj==0)
	{
		litera = stackcuvant.top();
		stackcuvant.pop();
		it = functii_Tranzitie.find(make_pair(tranzitie, litera));
		 if (it != functii_Tranzitie.end())
		{
			tranzitie = it->second;
		}
		else if (it == functii_Tranzitie.end()) //este blocaj
		{
			blocaj = 1;
			std::cout << "Este blocaj  :( ";
			break;

		}
		
	}
	if (cuvfinale.find(tranzitie) !=std::string::npos  && blocaj == 0)
	{
		std::cout << "Cuvant acceptat :) ";
	}
	else if (blocaj == 0 && cuvfinale.find(tranzitie) == std::string::npos)
	{
		std::cout << "Cuvant neacceptat :(";
	}

}

void AFDclass::verifica(bool &eroare)
{
	std::string cuvinsert;
	    eroare = 0;//starea initiala /cele finale fac parte din multimea de stari
	if (stari.find(stareinit) == stari.end()) //starea initiala se afla in multimea de stari
		eroare = 1;
	for (int i = 0; i < cuvfinale.size(); i++)
	{
		if (cuvfinale[i] != ',')
		{
			cuvinsert += cuvfinale[i];
		}
		if (cuvfinale[i] == ',')
		{
			if(stari.find(cuvinsert)==stari.end())
			eroare = 1;
			cuvinsert.clear();
		}
	}
	if (stari.find(cuvinsert) == stari.end())
	   eroare = 1;
	//tranzitiile contin doar el ale automatului
	std::set<std::string> toate_cuvintele;
	for (auto it = functii_Tranzitie.begin(); it != functii_Tranzitie.end(); ++it) //parcurg map ul
	{
		toate_cuvintele.insert(it->first.first);
		toate_cuvintele.insert(std::string(1,it->first.second));
		toate_cuvintele.insert(it->second);
	}
	//for (auto it = toate_cuvintele.begin(); it != toate_cuvintele.end(); ++it)
	//{
	//	if ((stari.find(*it) == stari.end())&& (set_alfabet.find(*it) == set_alfabet.end())) //&&cuvalfabet.find
	//	{
	//		eroare = 1;
	//	}
	//}
	if (eroare == 1)
		std::cout << "!!! EROARE";
}
AFDclass::AFDclass()
{

}

void AFDclass::citire()
{   std::string tranzitie1, tranzitie3;
	char tranzitie2;
	std::string stringStari;
	std::string cuvinsert;
	//f >> stringStari; //toate starile
	//for (int i = 0; i < stringStari.size(); i++)
	//{
	//	if (stringStari[i] != ',')
	//	{
	//		cuvinsert += stringStari[i];
	//	}
	//	if (stringStari[i] == ',')
	//	{
	//		stari.insert(cuvinsert);
	//		cuvinsert.clear();
	//	}
	//}
	//stari.insert(cuvinsert);
	cuvinsert.clear();
	//f >> alfabet_intrare; //alfabetul de intrare ca string
	//for (int i = 0; i < alfabet_intrare.size(); i++)
	//{
	//	if (alfabet_intrare[i] != ',')
	//	{
	//		cuvinsert += alfabet_intrare[i];
	//	}
	//	if (alfabet_intrare[i] == ',')
	//	{
	//		set_alfabet.insert(cuvinsert);
	//		cuvinsert.clear();
	//	}
	//}
	//set_alfabet.insert(cuvinsert);
	cuvinsert.clear();
	f >> stareinit; 
	f >> cuvfinale;//citeste stringul de finale
	f >> numarul_tranzitiilor;
	for (int i = 0; i < numarul_tranzitiilor; i++)
	{
		f >> tranzitie1 >> tranzitie2 >> tranzitie3;
		fct_tranzitie = std::make_pair(tranzitie1, tranzitie2);
		functii_Tranzitie.insert({ fct_tranzitie,tranzitie3 });
		//FunctiiTranzitie.insert(fct_tranzitie, tranzitie3);




	}



}
