#include "AFN_class.h"
#include<fstream>
#include<string>
#include<iostream>
#include<map>
#include<stack>
#include<map>
std::ifstream f("DateAFN.in");
void AFN_class::citire()
{

	f >> toateStarile; //toate starile
	f >> alfabet_intrare; //alfabetul de intrare
	f >> stareinit; //citeste cuv initial
	f >> cuvfinale; //citeste cuv finale
	std::string tranzitie1, tranzitie3,cuvinsert;
	char tranzitie2;
	int numarul_tranzitiilor;
	f >> numarul_tranzitiilor;
	std::set<std::string> set_tranzitii;
	for (int i = 0; i < numarul_tranzitiilor; i++)
	{
		f >> tranzitie1 >> tranzitie2; 
		fct_tranzitie = std::make_pair(tranzitie1, tranzitie2);
		f >> tranzitie3;
		
		for (int i = 0; i < tranzitie3.size(); i++)
		{
			if (tranzitie3[i] != ',')
			{
				cuvinsert += tranzitie3[i];
			}
			if (tranzitie3[i] == ',')
			{
				set_tranzitii.insert(cuvinsert);
				cuvinsert.clear();
			} 
		}
		set_tranzitii.insert(cuvinsert);
		cuvinsert.clear();
		 
		functii_Tranzitie[fct_tranzitie]= set_tranzitii;
		set_tranzitii.clear();
	}
}

void AFN_class::back(std::stack<char> stackcuvant, std::string tranzitie)
{
	if (stackcuvant.size() == cuvantcitit.size() && tranzitie != stareinit)
	{
		stackcuvant.empty();
	}

	if (esteFinal(tranzitie) == true && stackcuvant.empty())
	{
		std::cout << tranzitie;
		std::cout << " -> Cuvantul este acceptat! :)";
		acceptat=1;
		std::string cuvant;
	}
	else if (esteFinal(tranzitie) != true && stackcuvant.empty())
	{
		neacceptat = 1;
	}
   std::map<std::pair<std::string, char>, std::set<std::string>>::iterator it;
	char litera;
	if (!stackcuvant.empty())
	{  char litera = stackcuvant.top();
		stackcuvant.pop();
		fct_tranzitie = std::make_pair(tranzitie, litera);
		it = functii_Tranzitie.find(fct_tranzitie); //it cauta perechea q ,a care ne trebuie
		if (it != functii_Tranzitie.end())
		{
			if (acceptat != 1)
			{
				for (auto it2 : it->second) //parcurgem  setul de tranzitii din dreapta
				{
					tranzitie = it2;
					back(stackcuvant, tranzitie);
				}
			}
		}
	}
	

}

bool AFN_class::esteFinal(std::string tranzitie)
{
	if (cuvfinale.find(tranzitie) != std::string::npos)
		return true;
	else return false;
}


void AFN_class::afisare()
{
	std::cout << "Starea initiala : " << stareinit << "\n";
	std::cout << "Multimea starilor finale : " << cuvfinale << "\n";
	std::cout << "Q : " << toateStarile << "\n";
	std::cout << "Sigma : " <<alfabet_intrare<<"\n";
	std::cout << " Tranzitii : " << "\n";
	for (auto it = functii_Tranzitie.begin(); it != functii_Tranzitie.end(); ++it)
	{
		std::cout << it->first.first << " " << it->first.second << " -> ";
		for(auto x:it->second)
		 std::cout<<x<<" ";
		std::cout << "\n";
	}
}
void AFN_class::verificare()
{
	std::string cuvinsert;
	eroare = 0;//starea initiala /cele finale fac parte din multimea de stari
	if (toateStarile.find(stareinit) == std::string::npos) //starea initiala se afla in multimea de stari
		eroare = 1;
	for (int i = 0; i < cuvfinale.size(); i++)
	{
		if (cuvfinale[i] != ',')
		{
			cuvinsert += cuvfinale[i];
		}
		if (cuvfinale[i] == ',')
		{
			if (toateStarile.find(cuvinsert) == std::string::npos)
				eroare = 1;
			cuvinsert.clear();
		}
	}
	if (toateStarile.find(cuvinsert) == std::string::npos)
		eroare = 1;
	//tranzitiile contin doar el ale automatului
	for (auto it = functii_Tranzitie.begin(); it != functii_Tranzitie.end(); ++it) //parcurg map ul
	{
		if (toateStarile.find(it->first.first) != std::string::npos && alfabet_intrare.find(it->first.second) != std::string::npos)
		{
			for (auto i : it->second)
			{
				if (toateStarile.find(i) == std::string::npos)
					eroare = 1;
			}
		}
		else
		{
			eroare = 1;
		}

	}
	if (eroare == 1)
		std::cout << "!!! EROARE \n";

}

bool AFN_class::acceptare(std::string cuvant)
{
	blocaj = 0;
	std::stack<char> stackcuvant;
	cuvantcitit = cuvant;
	for (int i = cuvant.size() - 1; i >= 0; i--)
	{
		stackcuvant.push(cuvant[i]); //imi pune cuvantul in stack
	}
	back(stackcuvant, stareinit);
	if (neacceptat == 0 && acceptat == 0)
	{
		blocaj = 1;
	}
	return acceptat;

}
