#pragma once
#include<map>
#include<set>
#include<string>
#include<stack>
#include<unordered_map>
class AFN_class
{
public: std::string alfabet_intrare,cuvfinale,cuvantcitit;
	  std::string stareinit,toateStarile;
	   std::pair<std::string, char> fct_tranzitie;
	  std::map<std::pair<std::string, char>, std::set<std::string>> functii_Tranzitie;
	  std::set<std::string> stari;
	  bool acceptat = 0;
	  bool blocaj = 0;
	  bool neacceptat = 0;
	  void citire();
	  void back(std::stack<char> stackcuvant, std::string tranzitie);
	  bool esteFinal(std::string);
	  void afisare();
	  void verificare();
	  bool acceptare(std::string cuvant);
	  bool eroare = 0;
	  
	  
};

